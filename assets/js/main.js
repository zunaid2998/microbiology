$(document).ready(function(){
	$('#owl-slider-mb').owlCarousel({
		singleItem: true,
    	navigation: false,
    	pagination: true,
    	autoPlay: true
	});
	
	/*var windowWidth = $(window).width();
	$('#owl-slider-mb img').css('width', windowWidth);
	$('#owl-slider-mb img').css('height', '450px');*/
	
	$('#general-information').on('hide.bs.collapse', function(){
		var icon = $(this).parent().find('i');
		icon.removeClass('glyphicon-minus').addClass('glyphicon-plus');
	});
	$('#general-information').on('show.bs.collapse', function(){
		var icon = $(this).parent().find('i');
		icon.removeClass('glyphicon-plus').addClass('glyphicon-minus');
	});
	

	
	$('#teaching').on('show.bs.collapse', function(){
		var icon = $(this).parent().find('i');
		icon.removeClass('glyphicon-plus').addClass('glyphicon-minus');
	});
	
	$('#teaching').on('hide.bs.collapse', function(){
		var icon = $(this).parent().find('i');
		icon.removeClass('glyphicon-minus').addClass('glyphicon-plus');
	});
	
	$('#research').on('show.bs.collapse', function(){
		var icon = $(this).parent().find('i');
		icon.removeClass('glyphicon-plus').addClass('glyphicon-minus');
	});
	
	$('#research').on('hide.bs.collapse', function(){
		var icon = $(this).parent().find('i');
		icon.removeClass('glyphicon-minus').addClass('glyphicon-plus');
	});
	
	$('#personnel').on('show.bs.collapse', function(){
		var icon = $(this).parent().find('i');
		icon.removeClass('glyphicon-plus').addClass('glyphicon-minus');
	});
	
	$('#personnel').on('hide.bs.collapse', function(){
		var icon = $(this).parent().find('i');
		icon.removeClass('glyphicon-minus').addClass('glyphicon-plus');
	});
}); 